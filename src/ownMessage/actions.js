import { DELETE_MESSAGE, POST_MESSAGE, EDIT_MESSAGE } from "./actionTypes";

export const postMessage = (text) => ({
    type: POST_MESSAGE,
    payload: text
})

export const deleteMessage = (id) => ({
    type: DELETE_MESSAGE,
    payload: id
})

export const editMessage = (id, text) => ({
    type: EDIT_MESSAGE,
    payload: {
        id, text
    }
})