import React from 'react';

class MessageInput extends React.Component {
    constructor() {
        super();
        this.state = {text: ''};
        this.postText = this.postText.bind(this);
        this.addMessage = this.addMessage.bind(this);
    }
    postText(e) {
        this.setState({...this.state, text: e.target.value})
    }
    addMessage(e) {
        e.preventDefault();
        this.props.postMessage(this.state.text);
        this.setState({...this.state, text: ''})
        
    }
    render() {
        return (
                <form className = "message-input" onSubmit = {this.addMessage}>
                    <div className ="message-input-text mb-3">
                        <label htmlFor="exampleFormControlTextarea1" className="form-label">Type your message</label>
                        <textarea value = {this.state.text} onChange = {this.postText} className ="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <div className="col-auto">
                        <button type="submit" className="btn btn-primary mb-3">Post</button>
                    </div>
               </form>
        )
    }
}

export default MessageInput;