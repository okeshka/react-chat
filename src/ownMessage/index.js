import React from 'react';
import OwnMessage from './ownMessageItem';
import MessageInput from "./messageInput";
import {postMessage, deleteMessage, editMessage} from "./actions";
import {connect} from "react-redux";

class OwnMessagesList extends React.Component {
    
    render() {
        const {deleteMessage, postMessage, editMessage} = this.props;
        const {posts} = this.props.ownMessage;
        return <>
                    {
                        posts && 
                        posts.map((post) => <OwnMessage 
                                                key = {post.id}
                                                text = {post.text} 
                                                time = {post.date} 
                                                deleteMessage = {deleteMessage}
                                                id = {post.id}
                                                editMessage = {editMessage}
                                                        
                                            />
                                )
                    } 
                    <MessageInput postMessage = {postMessage} />
               </>
    }
}

const mapStateToProps = (state) => {
	return {
		ownMessage: state.ownMessageReducer 
	}
};

export default connect(mapStateToProps, {postMessage, deleteMessage, editMessage})(OwnMessagesList);