import { POST_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from "./actionTypes";
import { nanoid } from "nanoid";

const initialState = {
    posts: []
}

export default function ownMessageReducer(state = initialState, action) {
    switch (action.type) {
        case POST_MESSAGE: {
            return { ...state, 
                posts: [...state.posts, {text: action.payload, date: new Date(), id: nanoid(7)}]
            }
        }

        case DELETE_MESSAGE: {
            return {...state, posts: state.posts.filter((post) => post.id !== action.payload)}
        }
        
        case EDIT_MESSAGE: {
            return {...state, posts: state.posts
                .map((post) => 
                     { if (post.id === action.payload.id) post.text = action.payload.text; 
                        return post
                    })
            }
        }
        default:
            return state
    }
}

