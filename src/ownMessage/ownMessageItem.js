import React from "react";

class OwnMessage extends React.Component {

    render() {
    const {text, time, id} = this.props;
    const hours = time.getHours();
    const minutes = time.getMinutes();
        return (
            <div className = "own-message card" >
                <div className="card-body">
                    <p className = "message-text card-text">{text}</p>
                    <p className = "message-time">{hours}:{minutes}</p>
                    <button 
                        className = "message-edit btn btn-primary" 
                        onClick = {() => {
                            const text = prompt('Edit your message', this.props.text);
                            text && this.props.editMessage(id, text);
                        }}>
                            Edit
                    </button>
                    <button 
                        className = "message-delete btn btn-primary"
                        onClick = {() => this.props.deleteMessage(id)}>
                            Delete
                        </button>
                </div>
            </div>
        )
    }
}

export default OwnMessage;
