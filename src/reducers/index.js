import { combineReducers } from "redux";
import messageLike from '../message/reducer';
import chatReducer from "../chat/reducer";
import ownMessageReducer from "../ownMessage/reducer";

// import ownMessage from '../ownMessage/reducer';
// import modal from './modal/reducer';

const rootReducer = combineReducers({messageLike, chatReducer,
    ownMessageReducer,// modal
});

export default rootReducer;
