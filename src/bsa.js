import './App.css';
import Chat from './chat/index';


const URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

function App() {
  return (
      <Chat url = {URL} />
  );
}

export default App;
