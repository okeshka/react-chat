import React from 'react';
import "./header.css";

class Header extends React.Component {
       render() {
        const {title, usersCount, messagesCount, date} = this.props;
        return (
            <div className = "header">
                <div className = "header-title">
                    <span>{title}</span>
                </div>
                <div className = "header-users-count">
                    <span>{usersCount} participants</span>
                </div>
                <div className = "header-messages-count">
                    <span>{messagesCount} messages</span>
                </div>
                <div className = "header-last-message-date">
                    <span>Last message at {date}</span>
                </div>
            </div>
        )
    }
}

export default Header;