import { SET_MESSAGES } from "./actionTypes";

const initialState = {
    data: [], 
    isLoading: true
}

export default function chatReducer(state = initialState, action) {
    switch (action.type) {
        case SET_MESSAGES: {
            return { ...state, 
                     data: action.payload,
                     isLoading: false
                    }
        }
    
        default:
            return state
    }
}