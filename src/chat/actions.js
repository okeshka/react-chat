import { SET_MESSAGES } from "./actionTypes";

const setMessagesSucces = (data) => ({
    type: SET_MESSAGES,
    payload: data
})

export const setMessages = (url) => { 
    return (dispatch) => {
        fetch(url)
        .then((response) => response.json())
        .then((data) => {
            dispatch(setMessagesSucces(data));
        })
    }
} 
