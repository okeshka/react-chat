import React from "react";
import Preloader from "../common/preloader/preloader";
import Header from "../common/header/header";
import MessageList from "../message/index";
import OwnMessagesList from "../ownMessage/index";
import { setMessages } from "./actions";
import { connect } from "react-redux";

class Chat extends React.Component {
 
    componentDidMount() {
       this.props.setMessages(this.props.url)
    }
    render() {
        return this.props.message.isLoading ? <Preloader /> : (
            <div className = "container">
                <Header title = "my chat" usersCount = '45' messagesCount = '142' date = '15.02.2021 13:35' />
                <MessageList data = {this.props.message.data} />
                <OwnMessagesList />
            </div>
        )
    }
}
  
const mapStateToProps = (state) => {
	return {
		message: state.chatReducer
	}
};

// const mapDispatchToProps = dispatch => {
// 	return {
//         onSetMessages: url => {
//             dispatch(setMessages(url))
//         }
//     }
// };

export default connect(mapStateToProps, {setMessages})(Chat);