import { SET_LIKE } from "./actionTypes";

const initialState = {
    count: 0
}

export default function messageLike (state = initialState, action) {
    switch (action.type) {
        case SET_LIKE: {
            return {...state, count: state.count + 1}
        }
    
        default:
            return state
    }
}