import React from "react";
import Message from "./messageItem";

class MessageList extends React.Component {
    render() {
    const {data} = this.props;
        return <>
                    {data
                        .map((message) => 
                            <Message 
                                key = {message.id} 
                                text = {message.text} 
                                time = {message.createdAt} 
                                user = {message.user}
                                avatar = {message.avatar}
                            />
                    )
                    }
                </>
    }
}

export default MessageList;
