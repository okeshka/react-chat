import React from "react";
import { connect } from "react-redux";
import { like } from "./actions";
import './message.css';

class Message extends React.Component {
    
    render() {
    const {text, time, user, avatar} = this.props;
    const messageDate = new Date(time);
    const hours = messageDate.getHours();
    const minutes = messageDate.getMinutes();
    return (
        <div className = "message card" >
            <div className = "message-user-avatar card-img-top"><img src = {avatar} alt = "user avatar" /></div>
            <div className="card-body">
                <p className = "message-user-name card-title">{user}</p>
                <p className = "message-text card-text">{text}</p>
                <p className = "message-time">{hours}:{minutes}</p>
                <button className = "message-like btn btn-primary" onClick={() => this.props.like()}>Like!</button>
                <div>{this.props.message.count ? <span> {this.props.message.count} like</span> : '0 like'}</div>
            </div>
        </div>
    )
    }
}

const mapStateToProps = (state) => {
	return {
		message: state.messageLike
	}
};

export default connect(mapStateToProps, {like})(Message);
